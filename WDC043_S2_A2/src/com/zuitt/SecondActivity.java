package com.zuitt;

import java.util.ArrayList;
import java.util.HashMap;

public class SecondActivity {

    public static void main(String[] args){
        int[] primeArray = new int[5];

        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;

        System.out.println("The first prime number is: " + primeArray[0]);
        System.out.println("The second prime number is: " + primeArray[1]);
        System.out.println("The third prime number is: " + primeArray[2]);
        System.out.println("The fourth prime number is: " + primeArray[3]);
        System.out.println("The fifth prime number is: " + primeArray[4]);


        //ARRAY LIST
        ArrayList<String> friends = new ArrayList<>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);

    }
}
